﻿/* LO QUE SIGUE
arreglar valores tas tad tam y fc guardados para cada paciente y cada control porque no funcionan
lo que si funciona es guardar control_n para paciente 1 y 2. hacer para el resto de los 10 pacientes usando viewpac1_butpressed etc etc.

*/



//variables

var control_n = 0;
var VIEW_PAC = 0;
var view_paciente = 0;

var diu_total = 0;

var check_diu_sum = 0;

var diu_actual_value = 0;

var oldtime = 0;
var newtime = 0;


var TAM1 = 0;

//cont

var control_n = 0;
var control_nb = 0;
var control_nc = 0;
var control_nd = 0;
var control_ne = 0;
var control_nf = 0;
var control_ng = 0;
var control_nh = 0;
var control_ni = 0;
var control_nj = 0;

//PROMPTS VALUES
var TASVALUE;
var TADVALUE;
var FCVALUE;

//arrays control hemodinamico

//paciente 1
var control_hemodinamico_AR1 = [4];
var control_hemodinamico_AR2 = [4];
var control_hemodinamico_AR3 = [4];
var control_hemodinamico_AR4 = [4];
var control_hemodinamico_AR5 = [4];
//paciente 2
var control_hemodinamico_AR1B = [4];
var control_hemodinamico_AR2B = [4];
var control_hemodinamico_AR3B = [4];
var control_hemodinamico_AR4B = [4];
var control_hemodinamico_AR5B = [4];

var control_hemodinamico_n = 0;

var has_cont_hemodinamico_values = false;

// arrays control ritmo diuretico

var control_ritmodiu_AR1 = [7];





//clock
var get_h = 0;
var get_m = 0;

function volver () {
    window.location.href = "home_pase.html";
}

function home () {
    window.location.href = "index.html";
}

//DEBUG
document.getElementById("debug_vercontrolAR").addEventListener("click", function () {
    console.log(control_hemodinamico_AR1B);
});

document.getElementById("clear_localstorage").addEventListener("click", function () {
    localStorage.clear();
});

//boton volver





function load_controlnb_onload() {
    var load_controlnb = localStorage.getItem("save_controlnb");
    var load_controlnb2 = JSON.parse(load_controlnb);
    control_nb = load_controlnb2;
}

// ONLOAD
function ONLOADPACVIEW() {

    

    startTime();

    //cargar pacientes
    var load_Logp0 = localStorage.getItem("store logp0");
    var load_Logp0_b = JSON.parse(load_Logp0);
    Log_p0 = load_Logp0_b;
    console.log(load_Logp0_b);

    var load_Logp1 = localStorage.getItem("store logp1");
    var load_Logp1_b = JSON.parse(load_Logp1);
    Log_p1 = load_Logp1_b;
    console.log(load_Logp1_b);

    var load_Logp2 = localStorage.getItem("store logp2");
    var load_Logp2_b = JSON.parse(load_Logp2);
    Log_p2 = load_Logp2_b;
    console.log(load_Logp2_b);

    var load_Logp3 = localStorage.getItem("store logp3");
    var load_Logp3_b = JSON.parse(load_Logp3);
    Log_p3 = load_Logp3_b;
    console.log(load_Logp3_b);

    var load_Logp4 = localStorage.getItem("store logp4");
    var load_Logp4_b = JSON.parse(load_Logp4);
    Log_p4 = load_Logp4_b;
    console.log(load_Logp4_b);

    var load_Logp5 = localStorage.getItem("store logp5");
    var load_Logp5_b = JSON.parse(load_Logp5);
    Log_p5 = load_Logp5_b;
    console.log(load_Logp5_b);

    var load_Logp6 = localStorage.getItem("store logp6");
    var load_Logp6_b = JSON.parse(load_Logp6);
    Log_p6 = load_Logp6_b;
    console.log(load_Logp6_b);

    var load_Logp7 = localStorage.getItem("store logp7");
    var load_Logp7_b = JSON.parse(load_Logp7);
    Log_p7 = load_Logp7_b;
    console.log(load_Logp7_b);

    var load_Logp8 = localStorage.getItem("store logp8");
    var load_Logp8_b = JSON.parse(load_Logp8);
    Log_p8 = load_Logp8_b;
    console.log(load_Logp8_b);

    var load_Logp9 = localStorage.getItem("store logp9");
    var load_Logp9_b = JSON.parse(load_Logp9);
    Log_p9 = load_Logp9_b;
    console.log(load_Logp9_b);

    adddata_topage();

   

    //load stored control_n
    if (viewpac1_butpressed === true) {

        var load_controln = localStorage.getItem("strd_control_n");
        var load_controln2 = JSON.parse(load_controln);
        control_n = load_controln2;
        console.log(control_n + " control_n LOADED");

    } else if (viewpac2_butpressed === true) {

        var load_controln = localStorage.getItem("strd_control_n2");
        var load_controln2 = JSON.parse(load_controln);
        control_n = load_controln2;
        console.log(control_n + " control_n LOADED");

    } else {
        alert("load stored control_n error");
    }

    create_div_onload();

    viewing_patient();

    mon_tasbut1();
    mon_tadbut1();
    mon_fcbut1();

    mon_tasbut2();
    mon_tadbut2();
    mon_fcbut2();

    mon_tasbut3();
    mon_tadbut3();
    mon_fcbut3();

    mon_tasbut4();
    mon_tadbut4();
    mon_fcbut4();

    mon_tasbut5();
    mon_tadbut5();
    mon_fcbut5();

    load_controlnb_onload();

    //LOAD RITMO DIURETICO DATA
    if (control_nb === 1) {
        
        create_div_ritmodiu();

       
                document.getElementById("data_diu_inic").innerHTML = control_ritmodiu_AR1[0] + "ml";
            
                document.getElementById("data_diu_total").innerHTML = control_ritmodiu_AR1[4] + " ml";
      
                document.getElementById("data_diu_total").innerHTML = control_ritmodiu_AR1[5] + " ml";

        


            load_ritmodiu_inicdata();

        
     
    }

 

}




function set_control_number_onclick() {  //runs when hemodinamico is clicked

    var addcontrol = control_n + 1;
    control_n = addcontrol;
    console.log("control number " + control_n);

    save_control_n();

    //alert maximo controles
    if (control_n === 5) {
        alert("maxima cantidad de controles");
    }

    create_div_HEMODINAMICO();

    mon_tasbut1();
    mon_tadbut1();
    mon_fcbut1();

    mon_tasbut2();
    mon_tadbut2();
    mon_fcbut2();

    mon_tasbut3();
    mon_tadbut3();
    mon_fcbut3();

    mon_tasbut4();
    mon_tadbut4();
    mon_fcbut4();

    mon_tasbut5();
    mon_tadbut5();
    mon_fcbut5();

    

    
}

function set_control_numberb_onclick() {
    var addcontrol = control_nb + 1;
    control_nb = addcontrol;
    console.log("control number " + control_nb);
}


//crea div de control hemodinamico
function create_div_HEMODINAMICO() {


    var div_controles = document.getElementById("controles");// get div to append

    var cr_bu1 = document.createElement("button");
    var cr_bu2 = document.createElement("button");
    var cr_bu3 = document.createElement("button");//FC

    var cr_div = document.createElement("div"); //create div
    var cr_div2 = document.createElement("div");
    var cr_div3 = document.createElement("div");
    var cr_div4 = document.createElement("div");//TAD titulo
    var cr_div5 = document.createElement("div");
    var cr_div6 = document.createElement("div");
    var cr_p = document.createElement("p"); // create p

    var cr_p_tas_tit = document.createElement("p");//TAS titulo
    var cr_p_tas_val = document.createElement("p");//TAS value 

    var cr_p_tad_tit = document.createElement("p");//TAD titulo
    var cr_p_tad_val = document.createElement("p");//TAD value

    var cr_p_tam_tit = document.createElement("p");//TAM titulo
    var cr_p_tam_value = document.createElement("p");//TAM value

    var cr_p_fc_tit = document.createElement("p");//FC titulo
    var cr_p_fc_value = document.createElement("p");//FC value

    //CONTROL N
    div_controles.appendChild(cr_div);//append div to controles div
    cr_div.setAttribute("class", "form-control");
    cr_div.setAttribute("id", "control nº" + control_n);// crear control number



    //TITULO
    cr_div.appendChild(cr_div2);//div titulo
    cr_div2.setAttribute("id", "titulo" + control_n);
    cr_div2.setAttribute("class", "form-control text-center");
    document.getElementById("titulo" + control_n).innerHTML = "Control Hemodinamico Nº " + control_n;


    //agregar TAS button
    document.getElementById("titulo" + control_n).appendChild(cr_div3);//TAS
    cr_div3.setAttribute("id", "div_TAS" + control_n);
    cr_div3.setAttribute("class", "form-control");
    cr_div3.appendChild(cr_bu1);
    cr_bu1.setAttribute("id", "addtas_bu" + control_n);
    cr_bu1.setAttribute("class", "btn btn-primary btn-lg btn-block");
    document.getElementById("addtas_bu" + control_n).innerHTML = "Agregar TAS";

    //TAS titulo
    cr_div3.appendChild(cr_p_tas_tit);
    cr_p_tas_tit.setAttribute("id", "P TAS" + control_n);
    cr_p_tas_tit.setAttribute("class", "h5");
    document.getElementById("P TAS" + control_n).innerHTML = "Tension Arterial Sistolica";

    //TAS value
    cr_div3.appendChild(cr_p_tas_val);
    cr_p_tas_val.setAttribute("id", "TASval" + control_n);
  
    document.getElementById("TASval" + control_n).innerHTML = "TAS?";

    //agregar TAD button
    document.getElementById("titulo" + control_n).appendChild(cr_div4);//TAD
    cr_div4.setAttribute("id", "div_TAD" + control_n);
    cr_div4.setAttribute("class", "form-control");
    cr_div4.appendChild(cr_bu2);
    cr_bu2.setAttribute("id", "addtad_bu" + control_n);
    cr_bu2.setAttribute("class", "btn btn-primary btn-lg btn-block");
    document.getElementById("addtad_bu" + control_n).innerHTML = "Agregar TAD";

    //TAD titulo
    cr_div4.appendChild(cr_p_tad_tit);
    cr_p_tad_tit.setAttribute("id", "P TAD" + control_n);
    cr_p_tad_tit.setAttribute("class", "h5");
    document.getElementById("P TAD" + control_n).innerHTML = "Tension Arterial Diastolica";

    //TAD value
    cr_div4.appendChild(cr_p_tad_val);
    cr_p_tad_val.setAttribute("id", "TADval" + control_n);

    document.getElementById("TADval" + control_n).innerHTML = "TAD?";

    //TAM titulo
    document.getElementById("titulo" + control_n).appendChild(cr_div5);
    cr_div5.appendChild(cr_p_tam_tit);
    cr_p_tam_tit.setAttribute("id", "tamtit" + control_n);
    cr_p_tam_tit.setAttribute("class", "h5");
    document.getElementById("tamtit" + control_n).innerHTML = "Tension Arterial Media";

    //TAM value
    document.getElementById("titulo" + control_n).appendChild(cr_div5);
    cr_div5.appendChild(cr_p_tam_value);
    cr_p_tam_value.setAttribute("id", "tamval" + control_n);
    cr_p_tam_value.setAttribute("class", "value");
    document.getElementById("tamval" + control_n).innerHTML = TAM1;

    //agregar FC button
    document.getElementById("titulo" + control_n).appendChild(cr_div6);
    cr_div6.appendChild(cr_bu3);
    cr_bu3.setAttribute("id", "addfc_bu" + control_n);
    cr_bu3.setAttribute("class", "btn btn-info btn-lg btn-block")
    document.getElementById("addfc_bu" + control_n).innerHTML = "Agregar FC";

    //FC titulo
    document.getElementById("titulo" + control_n).appendChild(cr_div6);
    cr_div6.appendChild(cr_p_fc_tit);
    cr_p_fc_tit.setAttribute("id", "fctit" + control_n);
    cr_p_fc_tit.setAttribute("class", "h5");
    document.getElementById("fctit" + control_n).innerHTML = "Frecuencia Cardiaca";

    //FC value
    document.getElementById("titulo" + control_n).appendChild(cr_div6);
    cr_div6.appendChild(cr_p_fc_value);
    cr_p_fc_value.setAttribute("id", "fcval" + control_n);    
    document.getElementById("fcval" + control_n).innerHTML = "FC?";

    //hora
    cr_div.appendChild(cr_p);
    cr_p.setAttribute("id", "time control " + control_n);
    cr_p.setAttribute("class", "h5 text-center");
    document.getElementById("time control " + control_n).innerHTML = "Hora: " + get_h + ":" + get_m;

    //cont_hem_toAR();// todo ok 

}


//save control_n value to localstorage
function save_control_n() {

    if (viewpac1_butpressed === true){

    var save_contn = control_n;
    var save_contnb = JSON.stringify(save_contn);
    localStorage.setItem("strd_control_n", save_contnb);
    console.log("control_n saved");

    } else if (viewpac2_butpressed === true) {

        var save_contn = control_n;
        var save_contnb = JSON.stringify(save_contn);
        localStorage.setItem("strd_control_n2", save_contnb);
        console.log("control_n saved");

    } else {
        alert("save_control_n Error");
    }
}





//cantidad de divs creados on loa ddependiendo del valor de control_n
function create_div_onload() {

    if (control_n === 1) {

        control_hemodinamico_AR1[0] = 1;
      
        create_div_HEMODINAMICO();

        console.log("div 1 created");


    } else if (control_n === 2) {

        var rest_controln = control_n - 1;
        control_n = rest_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();

    } else if (control_n === 3) {
        var rest_controln = control_n - 2;
        control_n = rest_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();
    } else if (control_n === 4) {
        var rest_controln = control_n - 3;
        control_n = rest_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();
    } else if (control_n === 5) {
        var rest_controln = control_n - 4;
        control_n = rest_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();
    }


}

// create div ritmo diuretico

function create_div_ritmodiu() {

   

    var div_controles = document.getElementById("controles");// get div to append

    var cr_div = document.createElement("div"); //create div
    var cr_div_sum = document.createElement("div");
    var cr_div_data = document.createElement("div");
    var cr_p = document.createElement("p");
    var cr_p2 = document.createElement("p");
    var cr_p3 = document.createElement("p");
    var cr_p4 = document.createElement("p");
    var cr_p5 = document.createElement("p");
    var cr_p6 = document.createElement("p");
    var cr_p7 = document.createElement("p");
    var cr_p8 = document.createElement("p");
    var cr_p9 = document.createElement("p");
    var cr_p10 = document.createElement("p");
    var cr_br1 = document.createElement("BR");
    var cr_br2 = document.createElement("BR");
    var cr_br3 = document.createElement("BR");
    var cr_br4 = document.createElement("BR");
    var cr_br5 = document.createElement("BR");
    var cr_br6 = document.createElement("BR");
    var cr_br7 = document.createElement("BR");
    var p_ritdiutit = document.createElement("p");
    var p_ritdiu = document.createElement("p");
    var cr_hr1 = document.createElement("HR");
    var cr_hr2 = document.createElement("HR");
    var cr_hr3 = document.createElement("HR");
    var cr_hr4 = document.createElement("HR");

    var cr_sum_p1 = document.createElement("p");
    var cr_sum_p2 = document.createElement("p");//pending

    var cr_txt_input = document.createElement("INPUT");
    var cr_time_input = document.createElement("INPUT");
    var cr_date_input = document.createElement("INPUT");
    var cr_but1 = document.createElement("button");
    var cr_txt_input2 = document.createElement("INPUT");;

    var cr_but2 = document.createElement("button");

    div_controles.appendChild(cr_div);//append div to controles div
    cr_div.setAttribute("class", "form-control")
    cr_div.setAttribute("id", "control_ritmodiuretico");// crear control number

    //titulo
    cr_div.appendChild(cr_p);
    cr_p.setAttribute("id", "ritmodiu_titulo");
    cr_p.setAttribute("class", "h4 text-center");
    cr_p.innerHTML = "Ritmo Diuretico"

    //diuresis de inicio
    cr_div.appendChild(cr_p2);
    cr_p2.setAttribute("id", "diu_inicio");
    cr_p2.innerHTML = "Diuresis inicial (en ml)";
    cr_div.appendChild(cr_txt_input);
    cr_txt_input.setAttribute("id", "diu_inic_input");
    cr_txt_input.setAttribute("class","form-control")
    cr_txt_input.setAttribute("placeholder", "Ingresar en mililitros");

    cr_div.appendChild(cr_br1);

    //time
    cr_div.appendChild(cr_p8);
    cr_p8.innerHTML = "Fecha de inicio";
    cr_div.appendChild(cr_date_input);
    cr_date_input.setAttribute("type", "date");
    cr_date_input.setAttribute("class", "form-control");
    cr_date_input.setAttribute("id", "date1");

    cr_div.appendChild(cr_br2);

    cr_div.appendChild(cr_p3);
    cr_p3.innerHTML = "Hora de inicio";
    cr_div.appendChild(cr_time_input);
    cr_time_input.setAttribute("id", "time1");
    cr_time_input.setAttribute("class", "form-control");
    cr_time_input.setAttribute("type", "time");
    cr_time_input.setAttribute("value", "00:00");

    cr_div.appendChild(cr_br3);

    //boton ingresar datos de inicio
    cr_div.appendChild(cr_but1);
    cr_but1.setAttribute("id", "ritdiu_inicio_but");
    cr_but1.setAttribute("class", "btn btn-success btn-lg btn-block");
    cr_but1.innerHTML = "Aceptar parametros de inicio";

    cr_div.appendChild(cr_br4);

    //DATA
    cr_div.appendChild(cr_div_data);
    cr_div_data.setAttribute("class", "card text-center");
    cr_div_data.appendChild(cr_p4);
    cr_p4.setAttribute("class", "h2");
    cr_p4.innerHTML = "Diuresis de Inicio: ";
    cr_div_data.appendChild(cr_p5);
    cr_p5.setAttribute("id", "data_diu_inic");
    cr_p5.setAttribute("class", "h3");

    cr_div_data.appendChild(cr_hr1);

    cr_div_data.appendChild(cr_p6);
    cr_p6.setAttribute("class", "h2");
    cr_p6.innerHTML = "Hora de Inicio";
    cr_div_data.appendChild(cr_p7);
    cr_p7.setAttribute("id", "data_time_inic");
    cr_p7.setAttribute("class", "h4");

    cr_div_data.appendChild(cr_hr2);

    cr_div_data.appendChild(cr_p9);
    cr_p9.setAttribute("class", "h1");
    cr_p9.innerHTML = "Diuresis Total";

    cr_div_data.appendChild(cr_hr3);

    cr_div_data.appendChild(cr_p10);
    cr_p10.setAttribute("class", "h2");
    cr_p10.setAttribute("id", "data_diu_total");

    //div suma
    cr_div.appendChild(cr_div_sum);
    cr_div_sum.setAttribute("id", "ritmodiu_control_sum");
    cr_div_sum.setAttribute("class", "card");

    cr_div_sum.appendChild(cr_br5);

    cr_div_sum.appendChild(cr_sum_p1);
    cr_sum_p1.innerHTML = "Ingresar control de diuresis actual (el valor ingresado se suma a la diuresis de ingreso)";

    cr_div_sum.appendChild(cr_txt_input2);
    cr_txt_input2.setAttribute("type", "text");
    cr_txt_input2.setAttribute("id", "diu_actual");
    cr_txt_input2.setAttribute("class", "form-control");
    cr_txt_input2.setAttribute("placeholder", "diuresis actual en ml");

    cr_div_sum.appendChild(cr_br6);
  
    cr_div_sum.appendChild(cr_but2);
    cr_but2.setAttribute("class", "btn btn-dark btn-lg btn-block")
    cr_but2.setAttribute("id", "diu_actual_but");
    cr_but2.innerHTML = "Sumar Diuresis Actual";

    cr_div_sum.appendChild(cr_br7);

    cr_div_sum.appendChild(p_ritdiutit);
    p_ritdiutit.setAttribute("class", "h3 text-center")
    p_ritdiutit.innerHTML = "Ritmo Diuretico";

    cr_div_sum.appendChild(cr_hr4);

    cr_div_sum.appendChild(p_ritdiu);
    p_ritdiu.setAttribute("class", "h3 text-center");
    p_ritdiu.setAttribute("id", "ritdiu");

    control_nb = 1;
    //save control_nb
    var save_controlnb = control_nb;
    var save_controlnb2 = JSON.stringify(save_controlnb);
    localStorage.setItem("save_controlnb", save_controlnb2);


    mon_ritmodiu_but1();
    mon_ritmodiu_but2();

}



function mon_ritmodiu_but1() {

    var test_but = document.getElementById("control_ritmodiuretico");

    if (test_but) {
        document.getElementById("ritdiu_inicio_but").addEventListener("click", function () {

            control_ritmodiu_AR1[0] = diu_inic_input.value;
            control_ritmodiu_AR1[1] = time1.value;
            control_ritmodiu_AR1[2] = date1.value;
            control_ritmodiu_AR1[4] = diu_inic_input.value;

            diu_total = control_ritmodiu_AR1[0];

            console.log(diu_inic_input.value);
            console.log(time1.value);
            console.log("array " + control_ritmodiu_AR1);


            //add values to data de inicio
            document.getElementById("data_diu_inic").innerHTML = control_ritmodiu_AR1[0] + " ml";
            document.getElementById("data_time_inic").innerHTML = control_ritmodiu_AR1[1] + "hs " + control_ritmodiu_AR1[2];
            document.getElementById("data_diu_total").innerHTML = control_ritmodiu_AR1[0] + " ml";

            var init_time = control_ritmodiu_AR1[2] + " " + control_ritmodiu_AR1[1];
            var init_time_ms = Date.parse(init_time);
            //console.log(init_time);
            //console.log(init_time_ms);
            control_ritmodiu_AR1[3] = init_time_ms;

            var oldtime = init_time_ms;
            //console.log(oldtime);
            save_ritmodiu_initdata();


            /*var y = "2014-03-03 08:00";
            var x = Date.parse(y);
            var z = new Date(x);

            console.log(x);
            console.log(z);*/

        });
        
    }

}

function mon_ritmodiu_but2() {

    var test_but = document.getElementById("diu_actual_but");

    if (test_but) {

        document.getElementById("diu_actual_but").addEventListener("click", function () {


            if (control_ritmodiu_AR1[5] === undefined || control_ritmodiu_AR1[5] === 0) {
                diu_total = control_ritmodiu_AR1[0];
            } else {
                diu_total = control_ritmodiu_AR1[5];
            }

          

            var diu_input_value = document.getElementById("diu_actual").value;
            var diu1 = +diu_total + +diu_input_value;
            diu_total = diu1;

            document.getElementById("data_diu_total").innerHTML = diu_total + " ml";

            control_ritmodiu_AR1[5] = diu_total;

            control_ritmodiu_AR1[6] = true;

            save_ritmodiu_initdata();

            //add date

            var new_datetime = new Date();
            var time1 = new_datetime.getTime();
        

            //time difference

            var newtime = time1;

            oldtime = control_ritmodiu_AR1[3];
            console.log(oldtime);

            var diff = Math.abs(newtime - oldtime);
            console.log(diff);
            var horas_dif = +diff / 3600000;
            console.log(horas_dif);
  

            //mililistros hora

            var ml_hora = +diu_total / +horas_dif;
            console.log(ml_hora + " ml/hs");

            document.getElementById("ritdiu").innerHTML = ml_hora + " ml/hs";
        
            
                
        });
    }

  

}

function save_ritmodiu_initdata() {
    var save_diuinic = control_ritmodiu_AR1;
    var save_diuinic2 = JSON.stringify(save_diuinic);
    localStorage.setItem("save_inic_ritdiudata", save_diuinic2);
}

function load_ritmodiu_inicdata() {
    var load_diuinic = localStorage.getItem("save_inic_ritdiudata");
    var load_diuinic2 = JSON.parse(load_diuinic);
    control_ritmodiu_AR1 = load_diuinic2;
    console.log(control_ritmodiu_AR1);
}



/*function myFunction() {
    var minutes = 1000 * 60;
    var hours = minutes * 60;
    var days = hours * 24;
    var years = days * 365;
    var d = new Date();
    var t = d.getTime();

    var y = Math.round(t / hours);

    console.log(y);

    var h = new Date(50000);
    console.log(h);
 
}*/





function create_control_ritmodiu() {

    create_div_ritmodiu();
}

//monitors tas buts

//monitor tas but 1 press 
function mon_tasbut1() {
    var tasbut = document.getElementById("div_TAS1");

    if (tasbut) {
        document.getElementById("addtas_bu1").onclick = function () {
            TASVALUE = prompt("Ingresar Tension Arterial Sistolica");

            if (viewpac1_butpressed === true) {
                control_hemodinamico_AR1[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1 1 which is: " + control_hemodinamico_AR1[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac2_butpressed === true) {
                control_hemodinamico_AR1B[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1B 1 which is: " + control_hemodinamico_AR1B[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac3_butpressed === true) {
                control_hemodinamico_AR1C[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1C 1 which is: " + control_hemodinamico_AR1C[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac4_butpressed === true) {
                control_hemodinamico_AR1D[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1D 1 which is: " + control_hemodinamico_AR1D[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac5_butpressed === true) {
                control_hemodinamico_AR1E[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1E 1 which is: " + control_hemodinamico_AR1E[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac6_butpressed === true) {
                control_hemodinamico_AR1F[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1F 1 which is: " + control_hemodinamico_AR1F[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac7_butpressed === true) {
                control_hemodinamico_AR1G[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1G 1 which is: " + control_hemodinamico_AR1G[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac8_butpressed === true) {
                control_hemodinamico_AR1H[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1H 1 which is: " + control_hemodinamico_AR1H[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac9_butpressed === true) {
                control_hemodinamico_AR1I[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1I 1 which is: " + control_hemodinamico_AR1I[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac10_butpressed === true) {
                control_hemodinamico_AR1J[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1J 1 which is: " + control_hemodinamico_AR1J[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;
            }

            calc_TAM1();

          

                has_cont_hemodinamico_values = true;
                var save_hasvalue = has_cont_hemodinamico_values;
                var save_hasvalueb = JSON.stringify(save_hasvalue);
                localStorage.setItem("strd_cont_hem_hasvalue", save_hasvalueb);
            



            console.log("TAM to AR " + control_hemodinamico_AR1[3]);

            save_cont_hemTAS1();

        }
    }
}

function mon_tasbut2() {
    var tasbut = document.getElementById("div_TAS2");

    if (tasbut) {
        document.getElementById("addtas_bu2").onclick = function () {

            TASVALUE = prompt("Ingresar Tension Arterial Sistolica");

            if (viewpac1_butpressed === true){
              control_hemodinamico_AR2[1] = TASVALUE;
              console.log(TASVALUE + " is saved to AR2 1 which is: " + control_hemodinamico_AR2[1]);
              document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac2_butpressed === true) {
                control_hemodinamico_AR2B[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2B 1 which is: " + control_hemodinamico_AR2B[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac3_butpressed === true) {
                control_hemodinamico_AR2C[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2C 1 which is: " + control_hemodinamico_AR2C[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac4_butpressed === true) {
                control_hemodinamico_AR2D[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2D 1 which is: " + control_hemodinamico_AR2D[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac5_butpressed === true) {
                control_hemodinamico_AR2E[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2E 1 which is: " + control_hemodinamico_AR2E[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac6_butpressed === true) {
                control_hemodinamico_AR2F[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2F 1 which is: " + control_hemodinamico_AR2F[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac7_butpressed === true) {
                control_hemodinamico_AR2G[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2G 1 which is: " + control_hemodinamico_AR2G[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac8_butpressed === true) {
                control_hemodinamico_AR2H[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2H 1 which is: " + control_hemodinamico_AR2H[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac9_butpressed === true) {
                control_hemodinamico_AR2I[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2I 1 which is: " + control_hemodinamico_AR2I[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac10_butpressed === true) {
                control_hemodinamico_AR2J[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2J 1 which is: " + control_hemodinamico_AR2J[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            }
            calc_TAM2();

            has_cont_hemodinamico_values = true;
            var save_hasvalue = has_cont_hemodinamico_values;
            var save_hasvalueb = JSON.stringify(save_hasvalue);
            localStorage.setItem("strd_cont_hem_hasvalue", save_hasvalueb);



            console.log("TAM to AR " + control_hemodinamico_AR2[3]);

            save_cont_hemTAS2();

        }
    }
}

function mon_tasbut3() {
    var tasbut = document.getElementById("div_TAS3");

    if (tasbut) {
        document.getElementById("addtas_bu3").onclick = function () {
            TASVALUE = prompt("Ingresar Tension Arterial Sistolica");

            if (viewpac1_butpressed === true) {
                control_hemodinamico_AR3[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR3 1 which is: " + control_hemodinamico_AR3[1]);
                document.getElementById("TASval3").innerHTML = TASVALUE;
            } else if (viewpac2_butpressed === true) {
                control_hemodinamico_AR2B[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2B 1 which is: " + control_hemodinamico_AR2B[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac3_butpressed === true) {
                control_hemodinamico_AR3C[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2C 1 which is: " + control_hemodinamico_AR2C[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac4_butpressed === true) {
                control_hemodinamico_AR2D[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2D 1 which is: " + control_hemodinamico_AR2D[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac5_butpressed === true) {
                control_hemodinamico_AR2E[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2E 1 which is: " + control_hemodinamico_AR2E[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac6_butpressed === true) {
                control_hemodinamico_AR2F[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2F 1 which is: " + control_hemodinamico_AR2F[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac7_butpressed === true) {
                control_hemodinamico_AR2G[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2G 1 which is: " + control_hemodinamico_AR2G[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac8_butpressed === true) {
                control_hemodinamico_AR2H[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2H 1 which is: " + control_hemodinamico_AR2H[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac9_butpressed === true) {
                control_hemodinamico_AR2I[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2I 1 which is: " + control_hemodinamico_AR2I[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac10_butpressed === true) {
                control_hemodinamico_AR2J[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2J 1 which is: " + control_hemodinamico_AR2J[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            }

            calc_TAM3();

            has_cont_hemodinamico_values = true;
            var save_hasvalue = has_cont_hemodinamico_values;
            var save_hasvalueb = JSON.stringify(save_hasvalue);
            localStorage.setItem("strd_cont_hem_hasvalue", save_hasvalueb);



            console.log("TAM to AR " + control_hemodinamico_AR3[3]);

            save_cont_hemTAS3();

        }
    }
}

function mon_tasbut4() {
    var tasbut = document.getElementById("div_TAS4");

    if (tasbut) {
        document.getElementById("addtas_bu4").onclick = function () {
            TASVALUE = prompt("Ingresar Tension Arterial Sistolica");

            if (viewpac1_butpressed === true){
            control_hemodinamico_AR4[1] = TASVALUE;
            console.log(TASVALUE + " is saved to AR4 1 which is: " + control_hemodinamico_AR4[1]);
            document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac2_butpressed === true) {
                control_hemodinamico_AR4B[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4B 1 which is: " + control_hemodinamico_AR4B[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac3_butpressed === true) {
                control_hemodinamico_AR4C[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4C 1 which is: " + control_hemodinamico_AR4C[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac4_butpressed === true) {
                control_hemodinamico_AR4D[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4D 1 which is: " + control_hemodinamico_AR4D[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac5_butpressed === true) {
                control_hemodinamico_AR4E[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4E 1 which is: " + control_hemodinamico_AR4E[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac6_butpressed === true) {
                control_hemodinamico_AR4F[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4F 1 which is: " + control_hemodinamico_AR4F[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac7_butpressed === true) {
                control_hemodinamico_AR4G[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4G 1 which is: " + control_hemodinamico_AR4G[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac8_butpressed === true) {
                control_hemodinamico_AR4H[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4H 1 which is: " + control_hemodinamico_AR4H[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac9_butpressed === true) {
                control_hemodinamico_AR4I[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4I 1 which is: " + control_hemodinamico_AR4I[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac10_butpressed === true) {
                control_hemodinamico_AR4J[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4J 1 which is: " + control_hemodinamico_AR4J[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            }

            calc_TAM4();

            has_cont_hemodinamico_values = true;
            var save_hasvalue = has_cont_hemodinamico_values;
            var save_hasvalueb = JSON.stringify(save_hasvalue);
            localStorage.setItem("strd_cont_hem_hasvalue", save_hasvalueb);



            console.log("TAM to AR " + control_hemodinamico_AR4[3]);

            save_cont_hemTAS4();

        }
    }
}

function mon_tasbut5() {
    var tasbut = document.getElementById("div_TAS5");

    if (tasbut) {
        document.getElementById("addtas_bu5").onclick = function () {
            TASVALUE = prompt("Ingresar Tension Arterial Sistolica");


            if (viewpac1_butpressed === true){
            control_hemodinamico_AR5[1] = TASVALUE;
            console.log(TASVALUE + " is saved to AR5 1 which is: " + control_hemodinamico_AR5[1]);
            document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac2_butpressed === true) {
                control_hemodinamico_AR5B[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5B 1 which is: " + control_hemodinamico_AR5B[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac3_butpressed === true) {
                control_hemodinamico_AR5C[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5C 1 which is: " + control_hemodinamico_AR5C[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac4_butpressed === true) {
                control_hemodinamico_AR5D[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5D 1 which is: " + control_hemodinamico_AR5D[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac5_butpressed === true) {
                control_hemodinamico_AR5E[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5E 1 which is: " + control_hemodinamico_AR5E[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac6_butpressed === true) {
                control_hemodinamico_AR5F[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5F 1 which is: " + control_hemodinamico_AR5F[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac7_butpressed === true) {
                control_hemodinamico_AR5G[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5G 1 which is: " + control_hemodinamico_AR5G[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac8_butpressed === true) {
                control_hemodinamico_AR5H[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5H 1 which is: " + control_hemodinamico_AR5H[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac9_butpressed === true) {
                control_hemodinamico_AR5I[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5I 1 which is: " + control_hemodinamico_AR5I[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac10_butpressed) {
                control_hemodinamico_AR5J[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5J 1 which is: " + control_hemodinamico_AR5J[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            }

            calc_TAM5();

            has_cont_hemodinamico_values = true;
            var save_hasvalue = has_cont_hemodinamico_values;
            var save_hasvalueb = JSON.stringify(save_hasvalue);
            localStorage.setItem("strd_cont_hem_hasvalue", save_hasvalueb);



            console.log("TAM to AR " + control_hemodinamico_AR5[3]);

            save_cont_hemTAS5();

        }
    }
}


//monitor tad buttons
//monitor tad but 1 press
function mon_tadbut1() {
    var tadbut = document.getElementById("div_TAD1");

    if (tadbut) {
        document.getElementById("addtad_bu1").onclick = function () {
            TADVALUE = prompt("Ingresar Tension Arterial Diastolica");

            if (viewpac1_butpressed === true) {
                control_hemodinamico_AR1[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac2_butpressed === true) {
                control_hemodinamico_AR1B[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac3_butpressed === true) {
                control_hemodinamico_AR1C[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac4_butpressed === true) {
                control_hemodinamico_AR1D[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac5_butpressed === true) {
                control_hemodinamico_AR1E[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac6_butpressed === true) {
                control_hemodinamico_AR1F[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac7_butpressed === true) {
                control_hemodinamico_AR1G[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac8_butpressed === true) {
                control_hemodinamico_AR1H[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac9_butpressed === true) {
                control_hemodinamico_AR1I[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac10_butpressed === true) {
                control_hemodinamico_AR1J[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            }

            calc_TAM1();



            console.log("TAM to AR " + control_hemodinamico_AR1[3]);

            save_cont_hemTAD1()
        }
    }
}

    function mon_tadbut2() {
        var tadbut = document.getElementById("div_TAD2");

        if (tadbut) {
            document.getElementById("addtad_bu2").onclick = function () {
                TADVALUE = prompt("Ingresar Tension Arterial Diastolica");

                if (viewpac1_butpressed === true){
                control_hemodinamico_AR2[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac2_butpressed === true) {
                    control_hemodinamico_AR2B[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac3_butpressed === true) {
                    control_hemodinamico_AR2C[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac4_butpressed === true) {
                    control_hemodinamico_AR2D[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac5_butpressed === true) {
                    control_hemodinamico_AR2E[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac6_butpressed === true) {
                    control_hemodinamico_AR2F[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac7_butpressed === true) {
                    control_hemodinamico_AR2G[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac8_butpressed === true) {
                    control_hemodinamico_AR2H[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac9_butpressed === true) {
                    control_hemodinamico_AR2I[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac10_butpressed === true){
                    control_hemodinamico_AR2J[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                }
                

                calc_TAM2();



                console.log("TAM to AR " + control_hemodinamico_AR2[3]);

                save_cont_hemTAD2()
            }
        }
}

    function mon_tadbut3() {
        var tadbut = document.getElementById("div_TAD3");

        if (tadbut) {
            document.getElementById("addtad_bu3").onclick = function () {
                TADVALUE = prompt("Ingresar Tension Arterial Diastolica");

                if (viewpac1_butpressed === true){
                control_hemodinamico_AR3[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac2_butpressed === true) {
                    control_hemodinamico_AR3B[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac3_butpressed === true) {
                    control_hemodinamico_AR3C[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac4_butpressed === true) {
                    control_hemodinamico_AR3D[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac5_butpressed === true) {
                    control_hemodinamico_AR3E[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac6_butpressed === true) {
                    control_hemodinamico_AR3F[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac7_butpressed === true) {
                    control_hemodinamico_AR3G[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac8_butpressed === true) {
                    control_hemodinamico_AR3H[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac9_butpressed === true) {
                    control_hemodinamico_AR3I[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac10_butpressed === true) {
                    control_hemodinamico_AR3J[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                }

                calc_TAM3();

                console.log("TAM to AR " + control_hemodinamico_AR3[3]);

                save_cont_hemTAD3()
            }
        }
    }

    function mon_tadbut4() {
        var tadbut = document.getElementById("div_TAD4");

        if (tadbut) {
            document.getElementById("addtad_bu4").onclick = function () {
                TADVALUE = prompt("Ingresar Tension Arterial Diastolica");

                if (viewpac1_butpressed === true){
                control_hemodinamico_AR4[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac2_butpressed === true) {
                    control_hemodinamico_AR4B[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac3_butpressed === true) {
                    control_hemodinamico_AR4C[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac4_butpressed === true) {
                    control_hemodinamico_AR4D[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac5_butpressed === true) {
                    control_hemodinamico_AR4E[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac6_butpressed === true) {
                    control_hemodinamico_AR4F[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac7_butpressed === true) {
                    control_hemodinamico_AR4G[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac8_butpressed === true) {
                    control_hemodinamico_AR4H[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac9_butpressed === true) {
                    control_hemodinamico_AR4I[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac10_butpressed === true) {
                    control_hemodinamico_AR4J[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                }
                
             

                calc_TAM4();



                console.log("TAM to AR " + control_hemodinamico_AR4[3]);

                save_cont_hemTAD4()
            }
        }
    }


    function mon_tadbut5() {
        var tadbut = document.getElementById("div_TAD5");

        if (tadbut) {
            document.getElementById("addtad_bu5").onclick = function () {
                TADVALUE = prompt("Ingresar Tension Arterial Diastolica");

                if (viewpac1_butpressed === true){
                control_hemodinamico_AR5[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac2_butpressed === true) {
                    control_hemodinamico_AR5B[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac3_butpressed === true) {
                    control_hemodinamico_AR5C[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac4_butpressed === true) {
                    control_hemodinamico_AR5D[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac5_butpressed === true) {
                    control_hemodinamico_AR5E[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac6_butpressed === true) {
                    control_hemodinamico_AR5F[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac7_butpressed === true) {
                    control_hemodinamico_AR5G[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac8_butpressed === true) {
                    control_hemodinamico_AR5H[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac9_butpressed === true) {
                    control_hemodinamico_AR5I[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac10_butpressed === true) {
                    control_hemodinamico_AR5J[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                }

                calc_TAM5();



                console.log("TAM to AR " + control_hemodinamico_AR5[3]);

                save_cont_hemTAD5()
            }
        }
    }



    //monitor fc buttons
    //monitor fc1 button
    function mon_fcbut1() {
        var fcbut = document.getElementById("addfc_bu1");

        if (fcbut) {
            document.getElementById("addfc_bu1").onclick = function () {
                FCVALUE = prompt("Ingresar Frecuencia Cardiaca");

                if (viewpac1_butpressed === true){
                control_hemodinamico_AR1[3] = FCVALUE;
                console.log(FCVALUE);
                document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac2_butpressed === true) {
                    control_hemodinamico_AR1B[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac3_butpressed === true) {
                    control_hemodinamico_AR1C[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac4_butpressed === true) {
                    control_hemodinamico_AR1D[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac5_butpressed === true) {
                    control_hemodinamico_AR1E[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac6_butpressed === true) {
                    control_hemodinamico_AR1F[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac7_butpressed === true) {
                    control_hemodinamico_AR1G[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac8_butpressed === true) {
                    control_hemodinamico_AR1H[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac9_butpressed === true) {
                    control_hemodinamico_AR1I[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac10_butpressed === true) {
                    control_hemodinamico_AR1J[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                }

                control_hemodinamico_AR1[3] = FCVALUE;

                save_cont_hemFC1();

            }
        }
    }

    function mon_fcbut2() {
        var fcbut = document.getElementById("addfc_bu2");

        if (fcbut) {
            document.getElementById("addfc_bu2").onclick = function () {
                FCVALUE = prompt("Ingresar Frecuencia Cardiaca");

                if (viewpac1_butpressed === true) {
                    control_hemodinamico_AR2[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                } else if (viewpac2_butpressed === true) {
                    control_hemodinamico_AR2B[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                } else if (viewpac3_butpressed === true) {
                    control_hemodinamico_AR2C[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                } else if (viewpac4_butpressed === true) {
                    control_hemodinamico_AR2D[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                } else if (viewpac5_butpressed === true) {
                    control_hemodinamico_AR2E[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                } else if (viewpac6_butpressed === true) {
                    control_hemodinamico_AR2F[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                }else if (viewpac7_butpressed === true) {
                     control_hemodinamico_AR2G[3] = FCVALUE;
                     console.log(FCVALUE);
                     document.getElementById("fcval2").innerHTML = FCVALUE;

                } else if (viewpac8_butpressed === true) {
                    control_hemodinamico_AR2H[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                } else if (viewpac9_butpressed === true) {
                    control_hemodinamico_AR2I[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                } else if (viewpac10_butpressed === true) {
                    control_hemodinamico_AR2J[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                }


                //control_hemodinamico_AR2[3] = FCVALUE;

                save_cont_hemFC2();

            }
        }
    }

    function mon_fcbut3() {
        var fcbut = document.getElementById("addfc_bu3");

        if (fcbut) {
            document.getElementById("addfc_bu3").onclick = function () {
                FCVALUE = prompt("Ingresar Frecuencia Cardiaca");

                if (viewpac1_butpressed === true){
                control_hemodinamico_AR3[3] = FCVALUE;
                console.log(FCVALUE);
                document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac2_butpressed === true) {
                    control_hemodinamico_AR3B[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac3_butpressed === true) {
                    control_hemodinamico_AR3C[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac4_butpressed === true) {
                    control_hemodinamico_AR3D[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac5_butpressed === true) {
                    control_hemodinamico_AR3E[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac6_butpressed === true) {
                    control_hemodinamico_AR3F[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac7_butpressed === true) {
                    control_hemodinamico_AR3G[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac8_butpressed === true) {
                    control_hemodinamico_AR3H[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac9_butpressed === true) {
                    control_hemodinamico_AR3I[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac10_butpressed === true) {
                    control_hemodinamico_AR3J[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                }
                
                //control_hemodinamico_AR3[3] = FCVALUE;

                save_cont_hemFC3();

            }
        }
    }

    function mon_fcbut4() {
        var fcbut = document.getElementById("addfc_bu4");

        if (fcbut) {
            document.getElementById("addfc_bu4").onclick = function () {
                FCVALUE = prompt("Ingresar Frecuencia Cardiaca");
                control_hemodinamico_AR4[3] = FCVALUE;
                console.log(FCVALUE);
                document.getElementById("fcval4").innerHTML = FCVALUE;

               // control_hemodinamico_AR4[3] = FCVALUE;

                save_cont_hemFC4();

            }
        }
    }

    function mon_fcbut5() {
        var fcbut = document.getElementById("addfc_bu5");

        if (fcbut) {
            document.getElementById("addfc_bu5").onclick = function () {
                FCVALUE = prompt("Ingresar Frecuencia Cardiaca");

                if (viewpac1_butpressed === true){
                control_hemodinamico_AR5[3] = FCVALUE;
                console.log(FCVALUE);
                document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac2_butpressed === true) {
                    control_hemodinamico_AR5B[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac3_butpressed === true) {
                    control_hemodinamico_AR5C[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac4_butpressed === true) {
                    control_hemodinamico_AR5D[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac5_butpressed === true) {
                    control_hemodinamico_AR5E[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac6_butpressed === true) {
                    control_hemodinamico_AR5F[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac7_butpressed === true) {
                    control_hemodinamico_AR5G[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac8_butpressed === true) {
                    control_hemodinamico_AR5H[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac9_butpressed === true) {
                    control_hemodinamico_AR5I[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac10_butpressed === true) {
                    control_hemodinamico_AR5J[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                }

                //control_hemodinamico_AR5[3] = FCVALUE;

                save_cont_hemFC5();

            }
        }
    }

    //CALCULAR TAM
    function calc_TAM1() {
        var c_tas = TASVALUE;
        var c_tad = TADVALUE;

        var c1 = c_tad * 2;
        console.log(c1);
        var c2 = +c1 + +c_tas;
        console.log(c2);
        var c3 = c2 / 3;
        console.log(c3);
        TAM1 = c3;
        console.log(TAM1);

        if (viewpac1_butpressed === true) {
            control_hemodinamico_AR1[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1[4];

        } else if (viewpac2_butpressed === true) {
            control_hemodinamico_AR1B[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1B[4];

        } else if (viewpac3_butpressed === true) {
            control_hemodinamico_AR1C[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1C[4];

        }else if (viewpac4_butpressed === true) {
             control_hemodinamico_AR1D[4] = TAM1;
             document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1D[4];

        } else if (viewpac5_butpressed === true) {
            control_hemodinamico_AR1E[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1E[4];

        } else if (viewpac6_butpressed === true) {
            control_hemodinamico_AR1F[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1F[4];

        } else if (viewpac7_butpressed === true) {
            control_hemodinamico_AR1G[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1G[4];

        } else if (viewpac8_butpressed === true) {
            control_hemodinamico_AR1H[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1H[4];

        } else if (viewpac9_butpressed === true) {
            control_hemodinamico_AR1I[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1I[4];

        } else if (viewpac10_butpressed === true) {
            control_hemodinamico_AR1J[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1J[4];

        }
      

        save_cont_hemTAM1();




    }

    function calc_TAM2() {
        var c_tas = TASVALUE;
        var c_tad = TADVALUE;

        var c1 = c_tad * 2;
        console.log(c1);
        var c2 = +c1 + +c_tas;
        console.log(c2);
        var c3 = c2 / 3;
        console.log(c3);
        TAM2 = c3;
        console.log(TAM2);


        if (viewpac1_butpressed === true) {
            control_hemodinamico_AR2[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2[4];

        } else if (viewpac2_butpressed === true) {
            control_hemodinamico_AR2B[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2B[4];

        } else if (viewpac3_butpressed === true) {
            control_hemodinamico_AR2C[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2C[4];

        } else if (viewpac4_butpressed === true) {
            control_hemodinamico_AR2D[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2D[4];

        } else if (viewpac5_butpressed === true) {
            control_hemodinamico_AR2E[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2E[4];

        } else if (viewpac6_butpressed === true) {
            control_hemodinamico_AR2F[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2F[4];

        } else if (viewpac7_butpressed === true) {
            control_hemodinamico_AR2G[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2G[4];

        } else if (viewpac8_butpressed === true) {
            control_hemodinamico_AR2H[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2H[4];

        } else if (viewpac9_butpressed === true) {
            control_hemodinamico_AR2I[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2I[4];

        } else if (viewpac10_butpressed === true) {
            control_hemodinamico_AR2J[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2J[4];

        }

        save_cont_hemTAM2();

    }

    function calc_TAM3() {
        var c_tas = TASVALUE;
        var c_tad = TADVALUE;

        var c1 = c_tad * 2;
        console.log(c1);
        var c2 = +c1 + +c_tas;
        console.log(c2);
        var c3 = c2 / 3;
        console.log(c3);
        TAM3 = c3;
        console.log(TAM3);

        if (viewpac1_butpressed === true) {
            control_hemodinamico_AR3[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3[4];

        } else if (viewpac2_butpressed === true) {
            control_hemodinamico_AR3B[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3B[4];

        } else if (viewpac3_butpressed === true) {
            control_hemodinamico_AR3C[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3C[4];

        } else if (viewpac4_butpressed === true) {
            control_hemodinamico_AR3D[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3D[4];

        } else if (viewpac5_butpressed === true) {
            control_hemodinamico_AR3E[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3E[4];

        } else if (viewpac6_butpressed === true) {
            control_hemodinamico_AR3F[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3F[4];

        } else if (viewpac7_butpressed === true) {
            control_hemodinamico_AR3G[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3G[4];

        } else if (viewpac8_butpressed === true) {
            control_hemodinamico_AR3H[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3H[4];

        } else if (viewpac9_butpressed === true) {
            control_hemodinamico_AR3I[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3I[4];

        } else if (viewpac10_butpressed === true) {
            control_hemodinamico_AR3J[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3J[4];

        }

        save_cont_hemTAM3();




    }

    function calc_TAM4() {
        var c_tas = TASVALUE;
        var c_tad = TADVALUE;

        var c1 = c_tad * 2;
        console.log(c1);
        var c2 = +c1 + +c_tas;
        console.log(c2);
        var c3 = c2 / 3;
        console.log(c3);
        TAM4 = c3;
        console.log(TAM4);

        if (viewpac1_butpressed === true){
        control_hemodinamico_AR4[4] = TAM4;
        document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4[4];

        } else if (viewpac2_butpressed === true) {
            control_hemodinamico_AR4B[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4B[4];

        } else if (viewpac3_butpressed === true) {
            control_hemodinamico_AR4C[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4C[4];

        } else if (viewpac4_butpressed === true) {
            control_hemodinamico_AR4D[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4D[4];

        } else if (viewpac5_butpressed === true) {
            control_hemodinamico_AR4E[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4E[4];

        } else if (viewpac6_butpressed === true) {
            control_hemodinamico_AR4F[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4F[4];

        } else if (viewpac7_butpressed === true) {
            control_hemodinamico_AR4G[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4G[4];

        } else if (viewpac8_butpressed === true) {
            control_hemodinamico_AR4H[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4H[4];

        } else if (viewpac9_butpressed === true) {
            control_hemodinamico_AR4I[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4I[4];

        } else if (viewpac10_butpressed === true) {
            control_hemodinamico_AR4J[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4J[4];

        }

        save_cont_hemTAM4();




    }


    function calc_TAM5() {
        var c_tas = TASVALUE;
        var c_tad = TADVALUE;

        var c1 = c_tad * 2;
        console.log(c1);
        var c2 = +c1 + +c_tas;
        console.log(c2);
        var c3 = c2 / 3;
        console.log(c3);
        TAM5 = c3;
        console.log(TAM5);

        if (viewpac1_butpressed === true) {
            control_hemodinamico_AR5[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5[4];

        } else if (viewpac2_butpressed === true) {
            control_hemodinamico_AR5B[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5B[4];

        } else if (viewpac3_butpressed === true) {
            control_hemodinamico_AR5C[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5C[4];

        } else if (viewpac4_butpressed === true) {
            control_hemodinamico_AR5D[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5D[4];

        } else if (viewpac5_butpressed === true) {
            control_hemodinamico_AR5E[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5E[4];

        } else if (viewpac6_butpressed === true) {
            control_hemodinamico_AR5F[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5F[4];

        } else if (viewpac7_butpressed === true) {
            control_hemodinamico_AR5G[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5G[4];

        } else if (viewpac8_butpressed === true) {
            control_hemodinamico_AR5H[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5H[4];

        } else if (viewpac9_butpressed === true) {
            control_hemodinamico_AR5I[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5I[4];

        } else if (viewpac10_butpressed === true) {
            control_hemodinamico_AR5J[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5J[4];

        }


        save_cont_hemTAM5();




    }



    //save control hemodinamico AR to local storage
    function save_cont_hemTAS1() {
        if (viewpac1_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1[1] + " SAVED");

        } else if (viewpac2_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1B[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1B", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1B[1] + " SAVED");

        } else if (viewpac3_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1C[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1C", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1C[1] + " SAVED");

        } else if (viewpac4_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1D[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1D", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1D[1] + " SAVED");

        } else if (viewpac5_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1E[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1E", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1E[1] + " SAVED");

        } else if (viewpac6_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1F[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1F", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1F[1] + " SAVED");

        } else if (viewpac7_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1G[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1G", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1G[1] + " SAVED");

        } else if (viewpac8_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1H[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1H", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1H[1] + " SAVED");

        } else if (viewpac9_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1I[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1I", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1I[1] + " SAVED");

        } else if (viewpac10_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1J[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1J", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1J[1] + " SAVED");

        }
    }

    function save_cont_hemTAD1() {

        if (viewpac1_butpressed === true){
        var save_conthemTAD1 = control_hemodinamico_AR1[2];
        var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
        localStorage.setItem("saved_cont_hemTAD1", save_conthemTAD1b);
        console.log(control_hemodinamico_AR1[2] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1B[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1B", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1B[2] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1C[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1C", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1C[2] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1D[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1D", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1D[2] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1E[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1E", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1E[2] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1F[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1F", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1F[2] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1G[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1G", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1G[2] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1H[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1H", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1H[2] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1I[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1I", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1I[2] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1J[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1J", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1J[2] + " SAVED");

        }
    }

    function save_cont_hemFC1() {

        if (viewpac1_butpressed === true){
        var save_conthemFC1 = control_hemodinamico_AR1[3];
        var save_conthemFC1b = JSON.stringify(save_conthemFC1);
        localStorage.setItem("saved_cont_hemFC1", save_conthemFC1b);
        console.log(control_hemodinamico_AR1[3] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1B[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1B", save_conthemFC1b);
            console.log(control_hemodinamico_AR1B[3] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1C[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1C", save_conthemFC1b);
            console.log(control_hemodinamico_AR1C[3] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1D[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1D", save_conthemFC1b);
            console.log(control_hemodinamico_AR1D[3] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1E[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1E", save_conthemFC1b);
            console.log(control_hemodinamico_AR1E[3] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1F[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1F", save_conthemFC1b);
            console.log(control_hemodinamico_AR1F[3] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1G[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1G", save_conthemFC1b);
            console.log(control_hemodinamico_AR1G[3] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1H[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1H", save_conthemFC1b);
            console.log(control_hemodinamico_AR1H[3] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1I[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1I", save_conthemFC1b);
            console.log(control_hemodinamico_AR1I[3] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1J[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1J", save_conthemFC1b);
            console.log(control_hemodinamico_AR1J[3] + " SAVED");

        }

    }

    function save_cont_hemTAM1() {

        if (viewpac1_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1[4] + " SAVED");

        } else if (viewpac2_butpressed === true){
            var save_conthemTAM1 = control_hemodinamico_AR1B[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAMB1", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1B[4] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1C[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1C", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1C[4] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1D[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1D", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1D[4] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1E[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1E", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1E[4] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1F[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1F", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1F[4] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1G[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1G", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1G[4] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1H[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1H", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1H[4] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1I[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1I", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1I[4] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1J[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1J", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1J[4] + " SAVED");
        }

    }

    function save_cont_hemTAS2() {

        if (viewpac1_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2[1] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2B[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2B", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2B[1] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2C[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2C", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2C[1] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2D[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2D", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2D[1] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2E[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2E", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2E[1] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2F[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2F", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2F[1] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2G[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2G", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2G[1] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2H[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2H", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2H[1] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2I[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2I", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2I[1] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2J[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2J", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2J[1] + " SAVED");

        }
   
    }

    function save_cont_hemTAD2() {

        if (viewpac1_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2[2] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2B[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2B", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2B[2] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2C[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2C", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2C[2] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2D[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2D", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2D[2] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2E[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2E", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2E[2] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2F[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2F", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2F[2] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2G[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2G", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2G[2] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2H[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2H", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2H[2] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2I[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2I", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2I[2] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2J[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2J", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2J[2] + " SAVED");

        }

    }

    function save_cont_hemFC2() {

        if (viewpac1_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2", save_conthemFC2b);
            console.log(control_hemodinamico_AR2[3] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2B[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2B", save_conthemFC2b);
            console.log(control_hemodinamico_AR2B[3] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2C[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2C", save_conthemFC2b);
            console.log(control_hemodinamico_AR2C[3] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2D[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2D", save_conthemFC2b);
            console.log(control_hemodinamico_AR2D[3] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2E[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2E", save_conthemFC2b);
            console.log(control_hemodinamico_AR2E[3] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2F[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2F", save_conthemFC2b);
            console.log(control_hemodinamico_AR2F[3] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2G[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2G", save_conthemFC2b);
            console.log(control_hemodinamico_AR2G[3] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2H[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2H", save_conthemFC2b);
            console.log(control_hemodinamico_AR2H[3] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2I[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2I", save_conthemFC2b);
            console.log(control_hemodinamico_AR2I[3] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2J[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2J", save_conthemFC2b);
            console.log(control_hemodinamico_AR2J[3] + " SAVED");

        }
 

    }

    function save_cont_hemTAM2() {

        if (viewpac1_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        }


    }

    function save_cont_hemTAS3() {

        if (viewpac1_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3[1] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3B[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3B", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3B[1] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3C[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3C", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3C[1] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3D[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3D", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3D[1] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3E[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3E", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3E[1] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3F[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3F", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3F[1] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3G[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3G", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3G[1] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3H[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3H", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3H[1] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3I[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3I", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3I[1] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3J[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3J", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3J[1] + " SAVED");

        }
     
    }

    function save_cont_hemTAD3() {

        if (viewpac1_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3[2] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3B[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3B", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3B[2] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3C[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3C", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3C[2] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3D[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3D", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3D[2] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3E[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3E", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3E[2] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3F[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3F", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3F[2] + " SAVED");
                
        } else if (viewpac7_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3G[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3G", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3G[2] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3H[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3H", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3H[2] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3I[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3I", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3I[2] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3J[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3J", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3J[2] + " SAVED");

        }

    }


    function save_cont_hemFC3() {

        if (viewpac1_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3", save_conthemFC3b);
            console.log(control_hemodinamico_AR3[3] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3B[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3B", save_conthemFC3b);
            console.log(control_hemodinamico_AR3B[3] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3C[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3C", save_conthemFC3b);
            console.log(control_hemodinamico_AR3C[3] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3D[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3D", save_conthemFC3b);
            console.log(control_hemodinamico_AR3D[3] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3E[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3E", save_conthemFC3b);
            console.log(control_hemodinamico_AR3E[3] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3F[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3F", save_conthemFC3b);
            console.log(control_hemodinamico_AR3F[3] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3G[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3G", save_conthemFC3b);
            console.log(control_hemodinamico_AR3G[3] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3H[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3H", save_conthemFC3b);
            console.log(control_hemodinamico_AR3H[3] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3I[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3I", save_conthemFC3b);
            console.log(control_hemodinamico_AR3I[3] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3J[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3J", save_conthemFC3b);
            console.log(control_hemodinamico_AR3J[3] + " SAVED");
        }
   

    }

//continuar a partir de aca
    function save_cont_hemTAM3() {
        var save_conthemTAM3 = control_hemodinamico_AR3[4];
        var save_conthemTAM3b = JSON.stringify(save_conthemTAM3);
        localStorage.setItem("saved_cont_hemTAM3", save_conthemTAM3b);
        console.log(control_hemodinamico_AR3[4] + " SAVED");

    }

    function save_cont_hemTAS4() {
        var save_conthemTAS4 = control_hemodinamico_AR4[1];
        var save_conthemTAS4b = JSON.stringify(save_conthemTAS4);
        localStorage.setItem("saved_cont_hemTAS4", save_conthemTAS4b);
        console.log(control_hemodinamico_AR4[1] + " SAVED");
    }

    function save_cont_hemTAD4() {
        var save_conthemTAD4 = control_hemodinamico_AR4[2];
        var save_conthemTAD4b = JSON.stringify(save_conthemTAD4);
        localStorage.setItem("saved_cont_hemTAD4", save_conthemTAD4b);
        console.log(control_hemodinamico_AR4[2] + " SAVED");
    }


    function save_cont_hemFC4() {
        var save_conthemFC4 = control_hemodinamico_AR4[3];
        var save_conthemFC4b = JSON.stringify(save_conthemFC4);
        localStorage.setItem("saved_cont_hemFC4", save_conthemFC4b);
        console.log(control_hemodinamico_AR4[3] + " SAVED");

    }

    function save_cont_hemTAM4() {
        var save_conthemTAM4 = control_hemodinamico_AR4[4];
        var save_conthemTAM4b = JSON.stringify(save_conthemTAM4);
        localStorage.setItem("saved_cont_hemTAM4", save_conthemTAM4b);
        console.log(control_hemodinamico_AR4[4] + " SAVED");

    }

    function save_cont_hemTAS5() {
        var save_conthemTAS5 = control_hemodinamico_AR5[1];
        var save_conthemTAS5b = JSON.stringify(save_conthemTAS5);
        localStorage.setItem("saved_cont_hemTAS5", save_conthemTAS5b);
        console.log(control_hemodinamico_AR5[1] + " SAVED");
    }

    function save_cont_hemTAD5() {
        var save_conthemTAD5 = control_hemodinamico_AR5[2];
        var save_conthemTAD5b = JSON.stringify(save_conthemTAD5);
        localStorage.setItem("saved_cont_hemTAD5", save_conthemTAD5b);
        console.log(control_hemodinamico_AR5[2] + " SAVED");
    }


    function save_cont_hemFC5() {
        var save_conthemFC5 = control_hemodinamico_AR5[3];
        var save_conthemFC5b = JSON.stringify(save_conthemFC5);
        localStorage.setItem("saved_cont_hemFC5", save_conthemFC5b);
        console.log(control_hemodinamico_AR5[3] + " SAVED");

    }

    function save_cont_hemTAM5() {
        var save_conthemTAM5 = control_hemodinamico_AR5[4];
        var save_conthemTAM5b = JSON.stringify(save_conthemTAM5);
        localStorage.setItem("saved_cont_hemTAM5", save_conthemTAM5b);
        console.log(control_hemodinamico_AR5[4] + " SAVED");

    }

    // load control hemodinamico AR from local storage
    function load_cont_hemTAS1() {

        if (viewpac1_butpressed === true) {

            var load_conthemTAS1 = localStorage.getItem("saved_cont_hemTAS1");
            console.log(load_conthemTAS1);
            var load_conthemTAS1b = JSON.parse(load_conthemTAS1);
            console.log(load_conthemTAS1b);
            control_hemodinamico_AR1[1] = load_conthemTAS1b;
            console.log(control_hemodinamico_AR1[1] + " LOADED");

            var tasval = document.getElementById("TASval1");

            if (tasval) {
                document.getElementById("TASval1").innerHTML = control_hemodinamico_AR1[1];

            }

        } else if (viewpac2_butpressed === true) {

            var load_conthemTAS1 = localStorage.getItem("saved_cont_hemTAS1B");
            console.log(load_conthemTAS1);
            var load_conthemTAS1b = JSON.parse(load_conthemTAS1);
            console.log(load_conthemTAS1b);
            control_hemodinamico_AR1B[1] = load_conthemTAS1b;
            console.log(control_hemodinamico_AR1B[1] + " LOADED");

            var tasval = document.getElementById("TASval1");

            if (tasval) {
                document.getElementById("TASval1").innerHTML = control_hemodinamico_AR1B[1];

            }

        } else {
            alert("load_cont_hem_TAS1 ERROR");
               }

    
    }

    function load_cont_hemTAD1() {

        if (viewpac1_butpressed === true) {
            var load_conthemTAD1 = localStorage.getItem("saved_cont_hemTAD1");
            console.log(load_conthemTAD1);
            var load_conthemTAD1b = JSON.parse(load_conthemTAD1);
            console.log(load_conthemTAD1b);
            control_hemodinamico_AR1[2] = load_conthemTAD1b;
            console.log(control_hemodinamico_AR1[1] + " LOADED");

            var tadval = document.getElementById("TADval1");

            if (tadval) {
                document.getElementById("TADval1").innerHTML = control_hemodinamico_AR1[2];
            }
        } else if (viewpac2_butpressed === true) {
            var load_conthemTAD1 = localStorage.getItem("saved_cont_hemTAD1B");
            console.log(load_conthemTAD1);
            var load_conthemTAD1b = JSON.parse(load_conthemTAD1);
            console.log(load_conthemTAD1b);
            control_hemodinamico_AR1B[2] = load_conthemTAD1b;
            console.log(control_hemodinamico_AR1B[1] + " LOADED");

            var tadval = document.getElementById("TADval1");

            if (tadval) {
                document.getElementById("TADval1").innerHTML = control_hemodinamico_AR1B[2];
            }
        }
 
    }

    function load_cont_hemFC1() {

        if (viewpac1_butpressed === true) {
            var load_conthemFC1 = localStorage.getItem("saved_cont_hemFC1");
            console.log(load_conthemFC1);
            var load_conthemFC1b = JSON.parse(load_conthemFC1);
            console.log(load_conthemFC1b);
            control_hemodinamico_AR1[3] = load_conthemFC1b;
            console.log(control_hemodinamico_AR1[3] + " LOADED");

            var fcval = document.getElementById("fcval1");

            if (fcval) {
                document.getElementById("fcval1").innerHTML = control_hemodinamico_AR1[3];
            }
        } else if (viewpac2_butpressed === true) {
            var load_conthemFC1 = localStorage.getItem("saved_cont_hemFC1B");
            console.log(load_conthemFC1);
            var load_conthemFC1b = JSON.parse(load_conthemFC1);
            console.log(load_conthemFC1b);
            control_hemodinamico_AR1B[3] = load_conthemFC1b;
            console.log(control_hemodinamico_AR1B[3] + " LOADED");

            var fcval = document.getElementById("fcval1");

            if (fcval) {
                document.getElementById("fcval1").innerHTML = control_hemodinamico_AR1B[3];
            }
        }
 
    }

    function load_cont_hemTAM1() {

        if (viewpac1_butpressed === true) {
            var load_conthemTAM1 = localStorage.getItem("saved_cont_hemTAM1");
            console.log(load_conthemTAM1);
            var load_conthemTAM1b = JSON.parse(load_conthemTAM1);
            console.log(load_conthemTAM1b);
            control_hemodinamico_AR1[4] = load_conthemTAM1b;
            console.log(control_hemodinamico_AR1[4] + " LOADED");

            var tamval = document.getElementById("tamval1");

            if (tamval) {

                document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1[4];
            }
        } else if (viewpac2_butpressed === true) {
            var load_conthemTAM1 = localStorage.getItem("saved_cont_hemTAM1B");
            console.log(load_conthemTAM1);
            var load_conthemTAM1b = JSON.parse(load_conthemTAM1);
            console.log(load_conthemTAM1b);
            control_hemodinamico_AR1B[4] = load_conthemTAM1b;
            console.log(control_hemodinamico_AR1B[4] + " LOADED");

            var tamval = document.getElementById("tamval1");

            if (tamval) {

                document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1B[4];
            }
        }
    
    }

    function load_cont_hemTAS2() {

        var load_conthemTAS2 = localStorage.getItem("saved_cont_hemTAS2");
        console.log(load_conthemTAS2);
        var load_conthemTAS2b = JSON.parse(load_conthemTAS2);
        console.log(load_conthemTAS2b);
        control_hemodinamico_AR2[1] = load_conthemTAS2b;
        console.log(control_hemodinamico_AR2[1] + " LOADED");

        var tasval = document.getElementById("TASval2");

        if (tasval) {

            document.getElementById("TASval2").innerHTML = control_hemodinamico_AR2[1];
        }

    }

    function load_cont_hemTAD2() {

        var load_conthemTAD2 = localStorage.getItem("saved_cont_hemTAD2");
        console.log(load_conthemTAD2);
        var load_conthemTAD2b = JSON.parse(load_conthemTAD2);
        console.log(load_conthemTAD2b);
        control_hemodinamico_AR2[2] = load_conthemTAD2b;
        console.log(control_hemodinamico_AR2[1] + " LOADED");

        var tadval = document.getElementById("TADval2");

        if (tadval) {

            document.getElementById("TADval2").innerHTML = control_hemodinamico_AR2[2];
        }
    }


    function load_cont_hemFC2() {

        var load_conthemFC2 = localStorage.getItem("saved_cont_hemFC2");
        console.log(load_conthemFC2);
        var load_conthemFC2b = JSON.parse(load_conthemFC2);
        console.log(load_conthemFC2b);
        control_hemodinamico_AR2[3] = load_conthemFC2b;
        console.log(control_hemodinamico_AR2[3] + " LOADED");

        var fcval = document.getElementById("fcval2");

        if (fcval) {

            document.getElementById("fcval2").innerHTML = control_hemodinamico_AR2[3];
        }
    }

    function load_cont_hemTAM2() {
        var load_conthemTAM2 = localStorage.getItem("saved_cont_hemTAM2");
        console.log(load_conthemTAM2);
        var load_conthemTAM2b = JSON.parse(load_conthemTAM2);
        console.log(load_conthemTAM2b);
        control_hemodinamico_AR2[4] = load_conthemTAM2b;
        console.log(control_hemodinamico_AR2[4] + " LOADED");

        var tamval = document.getElementById("tamval2");

        if (tamval) {

            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2[4];
        }
    }

    function load_cont_hemTAS3() {

        var load_conthemTAS3 = localStorage.getItem("saved_cont_hemTAS3");
        console.log(load_conthemTAS3);
        var load_conthemTAS3b = JSON.parse(load_conthemTAS3);
        console.log(load_conthemTAS3b);
        control_hemodinamico_AR3[1] = load_conthemTAS3b;
        console.log(control_hemodinamico_AR3[1] + " LOADED");

        var tasval = document.getElementById("TASval3");

        if (tasval) {

            document.getElementById("TASval3").innerHTML = control_hemodinamico_AR3[1];
        }

    }

    function load_cont_hemTAD3() {

        var load_conthemTAD3 = localStorage.getItem("saved_cont_hemTAD3");
        console.log(load_conthemTAD3);
        var load_conthemTAD3b = JSON.parse(load_conthemTAD3);
        console.log(load_conthemTAD3b);
        control_hemodinamico_AR3[2] = load_conthemTAD3b;
        console.log(control_hemodinamico_AR3[1] + " LOADED");

        var tadval = document.getElementById("TADval3");

        if (tadval) {

            document.getElementById("TADval3").innerHTML = control_hemodinamico_AR3[2];
        }
    }

    function load_cont_hemFC3() {

        var load_conthemFC3 = localStorage.getItem("saved_cont_hemFC3");
        console.log(load_conthemFC3);
        var load_conthemFC3b = JSON.parse(load_conthemFC3);
        console.log(load_conthemFC3b);
        control_hemodinamico_AR3[3] = load_conthemFC3b;
        console.log(control_hemodinamico_AR3[3] + " LOADED");

        var fcval = document.getElementById("fcval3");

        if (fcval) {

            document.getElementById("fcval3").innerHTML = control_hemodinamico_AR3[3];
        }
    }

    function load_cont_hemTAM3() {
        var load_conthemTAM3 = localStorage.getItem("saved_cont_hemTAM3");
        console.log(load_conthemTAM3);
        var load_conthemTAM3b = JSON.parse(load_conthemTAM3);
        console.log(load_conthemTAM3b);
        control_hemodinamico_AR3[4] = load_conthemTAM3b;
        console.log(control_hemodinamico_AR3[4] + " LOADED");

        var tamval = document.getElementById("tamval3");

        if (tamval) {

            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3[4];
        }
    }

    function load_cont_hemTAS4() {

        var load_conthemTAS4 = localStorage.getItem("saved_cont_hemTAS4");
        console.log(load_conthemTAS4);
        var load_conthemTAS4b = JSON.parse(load_conthemTAS4);
        console.log(load_conthemTAS4b);
        control_hemodinamico_AR4[1] = load_conthemTAS4b;
        console.log(control_hemodinamico_AR4[1] + " LOADED");

        var tasval = document.getElementById("TASval4");

        if (tasval) {

            document.getElementById("TASval4").innerHTML = control_hemodinamico_AR4[1];
        }

    }

    function load_cont_hemTAD4() {

        var load_conthemTAD4 = localStorage.getItem("saved_cont_hemTAD4");
        console.log(load_conthemTAD4);
        var load_conthemTAD4b = JSON.parse(load_conthemTAD4);
        console.log(load_conthemTAD4b);
        control_hemodinamico_AR4[2] = load_conthemTAD4b;
        console.log(control_hemodinamico_AR4[1] + " LOADED");

        var tadval = document.getElementById("TADval4");

        if (tadval) {

            document.getElementById("TADval4").innerHTML = control_hemodinamico_AR4[2];
        }
    }

    function load_cont_hemFC4() {

        var load_conthemFC4 = localStorage.getItem("saved_cont_hemFC4");
        console.log(load_conthemFC4);
        var load_conthemFC4b = JSON.parse(load_conthemFC4);
        console.log(load_conthemFC4b);
        control_hemodinamico_AR4[3] = load_conthemFC4b;
        console.log(control_hemodinamico_AR4[3] + " LOADED");

        var fcval = document.getElementById("fcval4");

        if (fcval) {

            document.getElementById("fcval4").innerHTML = control_hemodinamico_AR4[3];
        }
    }

    function load_cont_hemTAM4() {
        var load_conthemTAM4 = localStorage.getItem("saved_cont_hemTAM4");
        console.log(load_conthemTAM4);
        var load_conthemTAM4b = JSON.parse(load_conthemTAM4);
        console.log(load_conthemTAM4b);
        control_hemodinamico_AR4[4] = load_conthemTAM4b;
        console.log(control_hemodinamico_AR4[4] + " LOADED");

        var tamval = document.getElementById("tamval4");

        if (tamval) {

            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4[4];
        }
    }

    function load_cont_hemTAS5() {

        var load_conthemTAS5 = localStorage.getItem("saved_cont_hemTAS5");
        console.log(load_conthemTAS5);
        var load_conthemTAS5b = JSON.parse(load_conthemTAS5);
        console.log(load_conthemTAS5b);
        control_hemodinamico_AR5[1] = load_conthemTAS5b;
        console.log(control_hemodinamico_AR5[1] + " LOADED");

        var tasval = document.getElementById("TASval5");

        if (tasval) {

            document.getElementById("TASval5").innerHTML = control_hemodinamico_AR5[1];
        }

    }

    function load_cont_hemTAD5() {

        var load_conthemTAD5 = localStorage.getItem("saved_cont_hemTAD5");
        console.log(load_conthemTAD5);
        var load_conthemTAD5b = JSON.parse(load_conthemTAD5);
        console.log(load_conthemTAD5b);
        control_hemodinamico_AR5[2] = load_conthemTAD5b;
        console.log(control_hemodinamico_AR5[1] + " LOADED");

        var tadval = document.getElementById("TADval5");

        if (tadval) {

            document.getElementById("TADval5").innerHTML = control_hemodinamico_AR5[2];
        }
    }

    function load_cont_hemFC5() {

        var load_conthemFC5 = localStorage.getItem("saved_cont_hemFC5");
        console.log(load_conthemFC5);
        var load_conthemFC5b = JSON.parse(load_conthemFC5);
        console.log(load_conthemFC5b);
        control_hemodinamico_AR5[3] = load_conthemFC5b;
        console.log(control_hemodinamico_AR5[3] + " LOADED");

        var fcval = document.getElementById("fcval5");

        if (fcval) {

            document.getElementById("fcval5").innerHTML = control_hemodinamico_AR5[3];
        }
    }

    function load_cont_hemTAM5() {
        var load_conthemTAM5 = localStorage.getItem("saved_cont_hemTAM5");
        console.log(load_conthemTAM5);
        var load_conthemTAM5b = JSON.parse(load_conthemTAM5);
        console.log(load_conthemTAM5b);
        control_hemodinamico_AR5[4] = load_conthemTAM5b;
        console.log(control_hemodinamico_AR5[4] + " LOADED");

        var tamval = document.getElementById("tamval5");

        if (tamval) {

            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5[4];
        }
    }


// CONTINUAR DE ACA PARA ARRIBA

    //RELOJ
    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('txt').innerHTML =
            h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);

        get_h = h;
        get_m = m;

    }
    function checkTime(i) {
        if (i < 10) { i = "0" + i };  // add zero in front of numbers < 10
        return i;
    }

    function adddata_topage() {//carga la ficha del paciente

        //load which ver pac buton is clicked

        //pac1
        var load_verpac1but = localStorage.getItem("strd_pacbu1");
        var load_verpac1butb = JSON.parse(load_verpac1but);
        viewpac1_butpressed = load_verpac1butb;
        console.log(viewpac1_butpressed + " paciente 1");
        //pac2
        var load_verpac2but = localStorage.getItem("strd_pacbu2");
        var load_verpac2butb = JSON.parse(load_verpac2but);
        viewpac2_butpressed = load_verpac2butb;
        console.log(viewpac2_butpressed + " paciente 2");
        //pac3
        var load_verpac3but = localStorage.getItem("strd_pacbu3");
        var load_verpac3butb = JSON.parse(load_verpac3but);
        viewpac3_butpressed = load_verpac3butb;
        console.log(viewpac3_butpressed + " paciente 3");
        //pac4
        var load_verpac4but = localStorage.getItem("strd_pacbu4");
        var load_verpac4butb = JSON.parse(load_verpac4but);
        viewpac4_butpressed = load_verpac4butb;
        console.log(viewpac4_butpressed + " paciente 4");
        //pac5
        var load_verpac5but = localStorage.getItem("strd_pacbu5");
        var load_verpac5butb = JSON.parse(load_verpac5but);
        viewpac5_butpressed = load_verpac5butb;
        console.log(viewpac5_butpressed + " paciente 5");
        //pac6
        var load_verpac6but = localStorage.getItem("strd_pacbu6");
        var load_verpac6butb = JSON.parse(load_verpac6but);
        viewpac6_butpressed = load_verpac6butb;
        console.log(viewpac6_butpressed + " paciente 6");
        //pac7
        var load_verpac7but = localStorage.getItem("strd_pacbu7");
        var load_verpac7butb = JSON.parse(load_verpac7but);
        viewpac7_butpressed = load_verpac7butb;
        console.log(viewpac7_butpressed + " paciente 7");
        //pac8
        var load_verpac8but = localStorage.getItem("strd_pacbu8");
        var load_verpac8butb = JSON.parse(load_verpac8but);
        viewpac8_butpressed = load_verpac8butb;
        console.log(viewpac8_butpressed + " paciente 8");
        //pac9
        var load_verpac9but = localStorage.getItem("strd_pacbu9");
        var load_verpac9butb = JSON.parse(load_verpac9but);
        viewpac9_butpressed = load_verpac9butb;
        console.log(viewpac9_butpressed + " paciente 9");
        //pac10
        var load_verpac10but = localStorage.getItem("strd_pacbu10");
        var load_verpac10butb = JSON.parse(load_verpac10but);
        viewpac10_butpressed = load_verpac10butb;
        console.log(viewpac10_butpressed + " paciente 10");



        if (viewpac1_butpressed === true) {
            var adddata_logp0_cama = Log_p0[0];
            document.getElementById("cama").innerHTML = adddata_logp0_cama;

            var adddata_logp0_apellido = Log_p0[1];
            document.getElementById("apellido").innerHTML = adddata_logp0_apellido;

            var adddata_logp0_sexo = Log_p0[2];
            document.getElementById("sexo").innerHTML = adddata_logp0_sexo;
        } else if (viewpac2_butpressed === true) {
            var adddata_logp1_cama = Log_p1[0];
            document.getElementById("cama").innerHTML = adddata_logp1_cama;

            var adddata_logp1_apellido = Log_p1[1];
            document.getElementById("apellido").innerHTML = adddata_logp1_apellido;

            var adddata_logp1_sexo = Log_p1[2];
            document.getElementById("sexo").innerHTML = adddata_logp1_sexo;
        } else if (viewpac3_butpressed === true) {
            var adddata_logp2_cama = Log_p2[0];
            document.getElementById("cama").innerHTML = adddata_logp2_cama;

            var adddata_logp2_apellido = Log_p2[1];
            document.getElementById("apellido").innerHTML = adddata_logp2_apellido;

            var adddata_logp2_sexo = Log_p2[2];
            document.getElementById("sexo").innerHTML = adddata_logp2_sexo;
        } else if (viewpac4_butpressed === true) {
            var adddata_logp3_cama = Log_p3[0];
            document.getElementById("cama").innerHTML = adddata_logp3_cama;

            var adddata_logp3_apellido = Log_p3[1];
            document.getElementById("apellido").innerHTML = adddata_logp3_apellido;

            var adddata_logp3_sexo = Log_p3[2];
            document.getElementById("sexo").innerHTML = adddata_logp3_sexo;
        } else if (viewpac5_butpressed === true) {
            var adddata_logp4_cama = Log_p4[0];
            document.getElementById("cama").innerHTML = adddata_logp4_cama;

            var adddata_logp4_apellido = Log_p4[1];
            document.getElementById("apellido").innerHTML = adddata_logp4_apellido;

            var adddata_logp4_sexo = Log_p4[2];
            document.getElementById("sexo").innerHTML = adddata_logp4_sexo;
        } else if (viewpac6_butpressed === true) {
            var adddata_logp5_cama = Log_p5[0];
            document.getElementById("cama").innerHTML = adddata_logp5_cama;

            var adddata_logp5_apellido = Log_p5[1];
            document.getElementById("apellido").innerHTML = adddata_logp5_apellido;

            var adddata_logp5_sexo = Log_p5[2];
            document.getElementById("sexo").innerHTML = adddata_logp5_sexo;
        } else if (viewpac7_butpressed === true) {
            var adddata_logp6_cama = Log_p6[0];
            document.getElementById("cama").innerHTML = adddata_logp6_cama;

            var adddata_logp6_apellido = Log_p6[1];
            document.getElementById("apellido").innerHTML = adddata_logp6_apellido;

            var adddata_logp6_sexo = Log_p6[2];
            document.getElementById("sexo").innerHTML = adddata_logp6_sexo;
        } else if (viewpac8_butpressed === true) {
            var adddata_logp7_cama = Log_p7[0];
            document.getElementById("cama").innerHTML = adddata_logp7_cama;

            var adddata_logp7_apellido = Log_p7[1];
            document.getElementById("apellido").innerHTML = adddata_logp7_apellido;

            var adddata_logp7_sexo = Log_p7[2];
            document.getElementById("sexo").innerHTML = adddata_logp7_sexo;
        } else if (viewpac9_butpressed === true) {
            var adddata_logp8_cama = Log_p8[0];
            document.getElementById("cama").innerHTML = adddata_logp8_cama;

            var adddata_logp8_apellido = Log_p8[1];
            document.getElementById("apellido").innerHTML = adddata_logp8_apellido;

            var adddata_logp8_sexo = Log_p8[2];
            document.getElementById("sexo").innerHTML = adddata_logp8_sexo;
        } else if (viewpac10_butpressed === true) {
            var adddata_logp9_cama = Log_p9[0];
            document.getElementById("cama").innerHTML = adddata_logp9_cama;

            var adddata_logp9_apellido = Log_p9[1];
            document.getElementById("apellido").innerHTML = adddata_logp9_apellido;

            var adddata_logp9_sexo = Log_p9[2];
            document.getElementById("sexo").innerHTML = adddata_logp9_sexo;
        }

    }

function viewing_patient() {
    if (viewpac1_butpressed === true) {

        load_cont_hemTAS1();
        load_cont_hemTAD1();
        load_cont_hemFC1();
        load_cont_hemTAM1();

        load_cont_hemTAS2();
        load_cont_hemTAD2();
        load_cont_hemFC2();
        load_cont_hemTAM2();

        load_cont_hemTAS3();
        load_cont_hemTAD3();
        load_cont_hemFC3();
        load_cont_hemTAM3();

        load_cont_hemTAS4();
        load_cont_hemTAD4();
        load_cont_hemFC4();
        load_cont_hemTAM4();

        load_cont_hemTAS5();
        load_cont_hemTAD5();
        load_cont_hemFC5();
        load_cont_hemTAM5();

    } else if (viewpac2_butpressed === true) {

        load_cont_hemTAS1();
        load_cont_hemTAD1();
        load_cont_hemFC1();
        load_cont_hemTAM1();

        load_cont_hemTAS2();
        load_cont_hemTAD2();
        load_cont_hemFC2();
        load_cont_hemTAM2();

        load_cont_hemTAS3();
        load_cont_hemTAD3();
        load_cont_hemFC3();
        load_cont_hemTAM3();

        load_cont_hemTAS4();
        load_cont_hemTAD4();
        load_cont_hemFC4();
        load_cont_hemTAM4();

        load_cont_hemTAS5();
        load_cont_hemTAD5();
        load_cont_hemFC5();
        load_cont_hemTAM5();

    } else {
        alert("error");
            }
}



